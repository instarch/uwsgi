# Maintainer: Angel Velasquez <angvp@archlinux.org>
# Contributor: Vsevolod Balashov <vsevolod@balashov.name>, Kevin Zuber <uKev@knet.eu>
pkgname=uwsgi
pkgver=1.9.8
pkgrel=3
pkgdesc="a fast (pure C), self-healing, developer-friendly WSGI server"
arch=(i686 x86_64)
url="http://projects.unbit.it/$pkgname"
license=(GPL2)
depends=(libxml2 php-embed libyaml jansson)
makedepends=(gcc python{,2} python2-pip)
optdepends=(
  'python'
  'python2'
)
backup=(etc/uwsgi/emperor.ini)
source=(http://projects.unbit.it/downloads/$pkgname-$pkgver.tar.gz
        emperor.ini
        emperor.uwsgi.service)
md5sums=('f9844674deb5da5565620a53c4b230d1'
         '37ca1ab71451ca9b02ab3725b2209f06'
         'a9788b2f174a7ab38c58c702be4513e8')

build() {
  cd $srcdir/$pkgname-$pkgver

  python2.7 uwsgiconfig.py --plugin plugins/python core python27
  python3.3 uwsgiconfig.py --plugin plugins/python core python33
  python uwsgiconfig.py --plugin plugins/php
  python uwsgiconfig.py --build base
}

package() {
  cd $srcdir/$pkgname-$pkgver

  pip2 install -U uwsgitop --root=${pkgdir}

  install -d ${pkgdir}/etc/uwsgi/apps
  install -d ${pkgdir}/usr/bin
  install -d ${pkgdir}/usr/lib/uwsgi

  install -Dm755 uwsgi $pkgdir/usr/bin/uwsgi

  install -Dm755 *_plugin.so $pkgdir/usr/lib/uwsgi

  install -Dm644 $srcdir/emperor.ini $pkgdir/etc/uwsgi/emperor.ini
  install -Dm644 $srcdir/emperor.uwsgi.service $pkgdir/usr/lib/systemd/system/emperor.uwsgi.service
}

# vim:filetype=sh ts=2 sw=2 ai:
